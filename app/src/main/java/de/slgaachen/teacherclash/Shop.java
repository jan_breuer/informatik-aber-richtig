package de.slgaachen.teacherclash;
//Jan
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class Shop extends AppCompatActivity implements View.OnClickListener {
    float x1,x2,y1,y2;
    User user = new User(Shop.this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);

        bottomNavigationView.setSelectedItemId(R.id.nav_shop); //bottom navigation view
        bottomNavigationView.setItemIconTintList(null);

        ImageButton slot1 = findViewById(R.id.chest_shop1);
        slot1.setOnClickListener(this);

        ImageButton slot2 = findViewById(R.id.chest_shop2);
        slot2.setOnClickListener(this);

        ImageButton slot3 = findViewById(R.id.chest_shop3);
        slot3.setOnClickListener(this);

        TextView textView = findViewById(R.id.showMoney);
        textView.setText("Money: " + user.getMoney());

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.nav_home:
                        startActivity(new Intent(getApplicationContext(),Homescreen.class));
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        return true;

                    case R.id.nav_invent:
                        startActivity(new Intent(getApplicationContext(),Inventar.class));
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        TextView money = findViewById(R.id.showMoney);
        money.setText("Money: " + user.getMoney());
    }

    public boolean onTouchEvent(MotionEvent touchEvent){
        //ApiHandler apiHandler = new ApiHandler();
        //apiHandler.getNews();

        switch(touchEvent.getAction()){
            case MotionEvent.ACTION_DOWN:
                x1 = touchEvent.getX();
                y1 = touchEvent.getY();
                break;
            case MotionEvent.ACTION_UP:
                x2 = touchEvent.getX();
                y2 = touchEvent.getY();
                if(x1 > x2){
                    Intent i = new Intent(Shop.this, Homescreen.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
                break;
        }
        return false;
    }

    // Beim Auswählen einer der "Kisten" wird der Seltenheitsgrad der Chestopen Activty überwiesen (Blau,Lila,Gelb | common/epic/legendary)
    @Override
    public void onClick(View v) {
            if (v.getId() == R.id.chest_shop1) {
                if(user.getMoney() >= 500){
                    Intent i = new Intent(Shop.this, Chestopen.class);
                    i.putExtra("Seltenheit", 1);
                    startActivity(i);
                }
                else {
                    TextView kostenblau = findViewById(R.id.kosten_blau);
                    kostenblau.setText("ZU WENIG GELD");
                }
            } else if (v.getId() == R.id.chest_shop2) {
                if(user.getMoney() >= 1000) {
                    Intent i = new Intent(Shop.this, Chestopen.class);
                    i.putExtra("Seltenheit", 2);
                    startActivity(i);
                }
                else {
                    TextView kostenpink = findViewById(R.id.kosten_pink);
                    kostenpink.setText("ZU WENIG GELD");
                }
            } else if (v.getId() == R.id.chest_shop3) {
                if(user.getMoney() >= 1500) {
                    Intent i = new Intent(Shop.this, Chestopen.class);
                    i.putExtra("Seltenheit", 3);
                    startActivity(i);
                }
                else {
                    TextView kostengelb = findViewById(R.id.kosten_gelb);
                    kostengelb.setText("ZU WENIG GELD");
                }
            }
        }
}
