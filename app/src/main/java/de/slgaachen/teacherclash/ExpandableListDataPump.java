
package de.slgaachen.teacherclash;
//Noah
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListDataPump {
    public static HashMap<String, List<String>> getData() {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();

        List<String> Remy = new ArrayList<String>();
        Remy.add("Das Problem an der Kiste ist...");
        Remy.add("Seelenfrieden");

        List<String> Mulorz = new ArrayList<String>();
        Mulorz.add("Nerf");
        Mulorz.add("Heißer Kaffee");


        List<String> Weißenbach = new ArrayList<String>();
        Weißenbach.add("Handy einsammeln");
        Weißenbach.add("Regelheft lesen");


        expandableListDetail.put("Herr Remy", Remy);
        expandableListDetail.put("Herr Mulorz", Mulorz);
        expandableListDetail.put("Frau Weißenbach", Weißenbach);
        return expandableListDetail;
    }
}
