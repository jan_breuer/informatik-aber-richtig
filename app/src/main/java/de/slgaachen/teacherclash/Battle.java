package de.slgaachen.teacherclash;
//Jonathan
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.companion.AssociationRequest;
import android.companion.BluetoothDeviceFilter;
import android.companion.CompanionDeviceManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Pattern;

public class Battle extends AppCompatActivity {

    private static final String TAG = "BattleActivity";
    private static final String MY_UUID = "2698cad0-273e-4392-9927-05a788a992d5";
    private static final int REQUEST_CODE = 0;
    private BluetoothDevice btDevice;
    private ConnectedThread mmConnectedThread;
    private Handler mHandler;

    // TextViews and ImageViews used after the Connection got established
    private TextView playerAction;
    private TextView enemyAction;
    private TextView playerHealth;
    private TextView enemyHealth;
    private TextView turnView;
    private TextView attack1Name;
    private TextView attack2Name;
    private ImageView aktPlayerImg;
    private ImageView secondPlayerImg;
    private ImageView thirdPlayerImg;
    private ImageView aktEnemyImg;
    private ImageView secondEnemyImg;
    private ImageView thirdEnemyImg;

    // Arrays which holds Lehrer for the enemy, for the player and in general all Lehrer
    private final User user = new User(Battle.this);
    private Lehrer[] usedLehrerList;
    private Lehrer[] allLehrerList;
    private Lehrer[] usedEnemyLehrerList;
    private Integer[] healthLehrer;
    private Integer[] healthEnemyLehrer;

    private int phase = 0;
    private int countLehrer = 0;
    private int moneyCount = 0;

    // Variables used to decide who's turn it is and who can set an attack
    private boolean enemyHasAttacked = false;
    private boolean battleHasBegun = false;


    //TODO: Win/Lose screen anzeigen nach dem Battle
    //TODO: Überprüfung, ob die Daten verarbeitbar sind, wenn nicht Daten nochmal holen -> kurzes zwischenspeichern der Daten nach dem Senden im Falle eines Fehlers zum neu-Senden
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_battle);

        setLehrer();

        //Check if Bluetooth is supported and if Bluetooth is Enabled, if not -> ask to enable it
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Toast.makeText(this, getString(R.string.bluetooth_not_supported), Toast.LENGTH_SHORT).show();
            finish();
            //If Bluetooth isn't enabled yet, then request to enable it
        } else if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_CODE);
        }
    }

    //Fills the usedLehrerList with random Lehrer. It's only a test Method until
    //Lehrer Selection is working properly
    private void setLehrer() {
        //Changes the ArrayList lehrerList to an array to be able to acces information
        //quickly and in union with other arrays
        ArrayList<Lehrer> lehrerList = user.getLehrer();
        Object[] objectLehrerList = lehrerList.toArray();
        allLehrerList = new Lehrer[objectLehrerList.length];
        System.arraycopy(objectLehrerList, 0, allLehrerList, 0, objectLehrerList.length);

        usedLehrerList = new Lehrer[3];
        usedEnemyLehrerList = new Lehrer[3];
        healthLehrer = new Integer[3];
        healthEnemyLehrer = new Integer[3];

        //Randomly Selects Lehrer out of every Lehrer stored in the allLehrerArray and
        //makes sure that no Lehrer is selected twice
        //Also fills the health array for every Lehrer
        int first = 0, second = 0, third;
        for (int i = 0; i < usedLehrerList.length; i++) {
            Random rand = new Random();
            if (i == 0) {
                first = rand.nextInt(allLehrerList.length);
                usedLehrerList[i] = allLehrerList[first];
                healthLehrer[i] = usedLehrerList[i].health;
            } else if (i == 1) {
                second = rand.nextInt(allLehrerList.length);
                while (second == first) {
                    second = rand.nextInt(allLehrerList.length);
                }
                usedLehrerList[i] = allLehrerList[second];
                healthLehrer[i] = usedLehrerList[i].health;
            } else if (i == 2) {
                third = rand.nextInt(allLehrerList.length);
                while (third == first || third == second) {
                    third = rand.nextInt(allLehrerList.length);
                }
                usedLehrerList[i] = allLehrerList[third];
                healthLehrer[i] = usedLehrerList[i].health;
            }
        }
    }

    private void addMoneyForEnd(int attack){
        moneyCount += usedLehrerList[0].attackList.get(attack).amount;
    }

    //When the player clicked on the second Player Image, the Lehrer is swapped in the array
    //and the UI gets Updated
    public void onSecondPlayerImgClick(View view) {
        if ((enemyHasAttacked || battleHasBegun) && healthLehrer[1] > 0) {
            changeLehrerPositionInArray(1);
            changeLehrerAfterSwap(0);
            setImagesForPlayerLehrer();
        } else if (healthLehrer[1] <= 0) {
            runOnUiThread(() -> Toast.makeText(Battle.this, getString(R.string.lehrer_is_dead_message), Toast.LENGTH_SHORT).show());
        } else {
            runOnUiThread(() -> Toast.makeText(Battle.this, getString(R.string.not_your_turn_message), Toast.LENGTH_SHORT).show());
        }
    }

    //When the player clicked on the third Player Image, the Lehrer is swapped in the array
    //and the UI gets Updated
    public void onThirdPlayerImgClick(View view) {
        if ((enemyHasAttacked || battleHasBegun) && healthLehrer[2] > 0) {
            changeLehrerPositionInArray(2);
            changeLehrerAfterSwap(0);
            setImagesForPlayerLehrer();
        } else if (healthLehrer[2] <= 0) {
            runOnUiThread(() -> Toast.makeText(Battle.this, getString(R.string.lehrer_is_dead_message), Toast.LENGTH_SHORT).show());
        } else {
            runOnUiThread(() -> Toast.makeText(Battle.this, getString(R.string.not_your_turn_message), Toast.LENGTH_SHORT).show());
        }
    }

    //Swaps the position of the given Lehrer to the first position in the array
    //aswell as changing the health array in the same way
    private void changeLehrerPositionInArray(int which) {
        Lehrer temp = usedLehrerList[0];
        usedLehrerList[0] = usedLehrerList[which];
        usedLehrerList[which] = temp;

        int tmp = healthLehrer[0];
        healthLehrer[0] = healthLehrer[which];
        healthLehrer[which] = tmp;
    }

    //Called when Lehrer gets swapped out either because he's dead or
    //because the player selected the Lehrers image
    private void changeLehrerAfterSwap(int which) {
        playerAction.setText(usedLehrerList[0].lastName + getString(R.string.lehrer_entered_battle_message));
        playerHealth.setText(getString(R.string.lehrer_health) + healthLehrer[0]);
        attack1Name.setText(usedLehrerList[0].attackList.get(0).name);
        attack2Name.setText(usedLehrerList[0].attackList.get(1).name);
        battleHasBegun = false;
        enemyHasAttacked = false;
        changeTurnViewVisibility();

        //Sends the new current Lehrer to the Enemy so that he can update his UI and Arrays
        String newMsg = getString(R.string.bluetooth_message_new) + usedLehrerList[0].id;
        byte[] bytes = newMsg.getBytes(Charset.defaultCharset());
        mmConnectedThread.write(bytes);

        //Let's the Thread sleep for 30 seconds
        //Necessary to prevent overflow of the Bluetooth connection
        try {
            Thread.sleep(30);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Sometimes it's necessary to update the health of a Lehrer which isn't the current one

        if (which == 0) {
            String newHp = getString(R.string.bluetooth_message_hpl) + healthLehrer[which];
            byte[] bytes1 = newHp.getBytes(Charset.defaultCharset());
            mmConnectedThread.write(bytes1);
            //If so (which != 0) then we send 2 extra Messages with the new health of the current
            //Lehrer aswell as the health of Lehrer at position which
        } else {
            String newHp = getString(R.string.bluetooth_message_hpe) + which + healthLehrer[which];
            byte[] bytes1 = newHp.getBytes(Charset.defaultCharset());
            mmConnectedThread.write(bytes1);

            try {
                Thread.sleep(30);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            String newHp1 = getString(R.string.bluetooth_message_hpl) + healthLehrer[0];
            byte[] bytes2 = newHp1.getBytes(Charset.defaultCharset());
            mmConnectedThread.write(bytes2);
        }


    }

    //Updates the UI-Elements for the Enemy Lehrer after it got swaped
    private void changeEnemyLehrerAfterSwap() {
        enemyAction.setText(usedEnemyLehrerList[0].lastName + getString(R.string.lehrer_entered_battle_message));
        enemyHealth.setText(getString(R.string.lehrer_health) + healthEnemyLehrer[0]);

        setImagesForEnemyLehrer();
    }

    //Button for the first attack got clicked
    public void onAttack1Click(View view) {
        //Asks if it's the players turn, if so proceed, otherwise send a Toast saying it's
        //not the Players turn
        if (enemyHasAttacked || battleHasBegun) {
            setPlayerActionText(0);
            //If the attack is an Damage Attack, send the Attack to the Enemy for further calculation
            //otherwise (if it's an Healing Attack) calculate how much the Lehrer heals and afterwards
            //send the updated health to the Enemy and change turns (Enemy's turn begins)
            if (usedLehrerList[0].attackList.get(0).isDamage) {
                String string = getString(R.string.bluetooth_message_att) + "1";
                mmConnectedThread.write(string.getBytes(Charset.defaultCharset()));
            } else {
                healthLehrer[0] += usedLehrerList[0].attackList.get(0).amount;
                runOnUiThread(() -> playerHealth.setText(getString(R.string.lehrer_health) + healthLehrer[0]));

                String hpl = getString(R.string.bluetooth_message_hpl) + healthLehrer[0];
                mmConnectedThread.write(hpl.getBytes(Charset.defaultCharset()));

                try {
                    Thread.sleep(30);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                String cta = getString(R.string.bluetooth_message_cta) + "0";
                mmConnectedThread.write(cta.getBytes(Charset.defaultCharset()));
            }

            enemyHasAttacked = false;
            battleHasBegun = false;
            changeTurnViewVisibility();
            addMoneyForEnd(0);
        } else {
            Toast.makeText(Battle.this, getString(R.string.not_your_turn_message), Toast.LENGTH_SHORT).show();
        }
    }

    //Button for the second attack got clicked
    //Functions the same as Attack 1, unfortunately it isn't possible to give any further parameter
    //other then view, if it is an onClick Method for a Button. Otherwise the function would have been generic
    public void onAttack2Click(View view) {
        if (enemyHasAttacked || battleHasBegun) {
            setPlayerActionText(1);
            if (usedLehrerList[0].attackList.get(1).isDamage) {
                String string = getString(R.string.bluetooth_message_att) + "2";
                mmConnectedThread.write(string.getBytes(Charset.defaultCharset()));
            } else {
                healthLehrer[0] += usedLehrerList[0].attackList.get(1).amount;
                runOnUiThread(() -> playerHealth.setText(getString(R.string.lehrer_health) + healthLehrer[0]));

                String hpl = getString(R.string.bluetooth_message_hpl) + healthLehrer[0];
                mmConnectedThread.write(hpl.getBytes(Charset.defaultCharset()));

                try {
                    Thread.sleep(30);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                String cta = getString(R.string.bluetooth_message_cta) + "1";
                mmConnectedThread.write(cta.getBytes(Charset.defaultCharset()));
            }

            enemyHasAttacked = false;
            battleHasBegun = false;
            changeTurnViewVisibility();
            addMoneyForEnd(1);
        } else {
            Toast.makeText(Battle.this, getString(R.string.not_your_turn_message), Toast.LENGTH_SHORT).show();
        }
    }

    //Updates the Action of a Lehrer after Playing an attack
    private void setPlayerActionText(int attack) {
        if (usedLehrerList[0].attackList.get(attack).isDamage) {
            runOnUiThread(() -> playerAction.setText(usedLehrerList[0].lastName + getString(R.string.lehrer_uses_which_attack) + usedLehrerList[0].attackList.get(attack).name + getString(R.string.lehrer_does_damage_attack) + usedLehrerList[0].attackList.get(attack).amount + getString(R.string.lehrer_damage_attack)));
        } else {
            runOnUiThread(() -> playerAction.setText(usedLehrerList[0].lastName + getString(R.string.lehrer_uses_which_attack) + usedLehrerList[0].attackList.get(attack).name + getString(R.string.lehrer_heals_damage_attack) + usedLehrerList[0].attackList.get(attack).amount + getString(R.string.lehrer_heals_attack)));
        }
    }

    //Updates the Action of the Enemy player after attacking
    private void setEnemyActionText(int attack) {
        if (usedEnemyLehrerList[0].attackList.get(attack).isDamage) {
            runOnUiThread(() -> enemyAction.setText(usedEnemyLehrerList[0].lastName + getString(R.string.lehrer_uses_which_attack) + usedEnemyLehrerList[0].attackList.get(attack).name + getString(R.string.lehrer_does_damage_attack) + usedEnemyLehrerList[0].attackList.get(attack).amount + getString(R.string.lehrer_damage_attack)));
        } else {
            runOnUiThread(() -> enemyAction.setText(usedEnemyLehrerList[0].lastName + getString(R.string.lehrer_uses_which_attack) + usedEnemyLehrerList[0].attackList.get(attack).name + getString(R.string.lehrer_heals_damage_attack) + usedEnemyLehrerList[0].attackList.get(attack).amount + getString(R.string.lehrer_heals_attack)));
        }
    }


    //Calculates how much Damage the enemy deals and if the current Lehrer dies (less than 0 health)
    //swap it's position with another Lehrer which has more than 0 health
    private void calculateDmgDone(int attack, int aktHealth) {
        //Updates the Damage done by the enemy
        int healthLeft = aktHealth - usedEnemyLehrerList[0].attackList.get(attack).amount;
        healthLehrer[0] = healthLeft;
        runOnUiThread(() -> playerHealth.setText(getString(R.string.lehrer_health) + healthLehrer[0]));
        //If every Lehrer the Player has is dead (below/has 0 health) then let the enemy know he won and Exit the Game
        if (healthLehrer[0] <= 0 && healthLehrer[1] <= 0 && healthLehrer[2] <= 0) {
            greyOutImagePlayer(0);
            String winMsg = getString(R.string.bluetooth_message_win) + usedEnemyLehrerList[0].attackList.get(attack).name;
            byte[] bytes = winMsg.getBytes(Charset.defaultCharset());
            mmConnectedThread.write(bytes);
            gameOver(false);
            //Is the second Lehrer alive, but the first one dead? Then swap the second Lehrer with the first one, Update the UI
            //and inform the enemy
        } else if (healthLehrer[0] <= 0 && healthLehrer[1] > 0) {
            changeLehrerPositionInArray(1);
            changeLehrerAfterSwap(1);
            setImagesForPlayerLehrer();
            greyOutImagePlayer(1);
            playerHealth.setText(getString(R.string.lehrer_health) + healthLehrer[0]);
            //Is the third Lehrer still alive and the current dead? Then swap the third Lehrer with the first one, Update the UI
            //and inform the enemy
        } else if (healthLehrer[0] <= 0) {
            changeLehrerPositionInArray(2);
            changeLehrerAfterSwap(2);
            setImagesForPlayerLehrer();
            greyOutImagePlayer(2);
            playerHealth.setText(getString(R.string.lehrer_health) + healthLehrer[0]);
            //If the current Lehrer isn't dead, send the updated health of the Lehrer to the enemy
        } else {
            String hpl = getString(R.string.bluetooth_message_hpl) + healthLehrer[0];
            byte[] bytes = hpl.getBytes(Charset.defaultCharset());
            mmConnectedThread.write(bytes);
        }

    }

    //When the Battle is over, cancel the Bluetooth connection and return to the Homescreen
    private void gameOver(boolean won) {
        mmConnectedThread.cancel();
        Intent intent;
        if(won){
            intent = new Intent(Battle.this, winActivity.class);
            intent.putExtra("Money", moneyCount + 100);
            user.addMoney(moneyCount + 100);
        } else {
            intent = new Intent(Battle.this, lostActivity.class);
            intent.putExtra("Money", (moneyCount/2) + 50);
            user.addMoney((moneyCount/2) + 50);
        }
        user.safemoney();
        startActivity(intent);
    }

    //After the connection got Established, connect the different View attributes with their layout ID
    private void changeActivityLayoutAfterConnect() {
        setContentView(R.layout.activity_main_battle);
        playerAction = findViewById(R.id.textViewplayer);
        enemyAction = findViewById(R.id.textViewenemy);
        playerHealth = findViewById(R.id.lebenplayer);
        enemyHealth = findViewById(R.id.lebenenemy);
        aktPlayerImg = findViewById(R.id.player1);
        secondPlayerImg = findViewById(R.id.player2);
        thirdPlayerImg = findViewById(R.id.player3);
        aktEnemyImg = findViewById(R.id.enemy1);
        secondEnemyImg = findViewById(R.id.enemy2);
        thirdEnemyImg = findViewById(R.id.enemy3);
        turnView = findViewById(R.id.turnIndicator);
        attack1Name = findViewById(R.id.attacke1TV);
        attack2Name = findViewById(R.id.attacke2TV);
        changeTurnViewVisibility();
    }

    //Changes the Visibility of the turn Text
    private void changeTurnViewVisibility() {
        if (battleHasBegun || enemyHasAttacked) {
            turnView.setVisibility(View.VISIBLE);
        } else {
            turnView.setVisibility(View.INVISIBLE);
        }
    }

    //Used to indicate which Lehrer is dead, it simply blackens the images of the Lehrer
    //a little. It's only visually to help the Player understand which Lehrer he has left
    private void greyOutImagePlayer(int which) {
        if (which == 0) {
            aktPlayerImg.setColorFilter(Color.BLACK, PorterDuff.Mode.OVERLAY);
        }
        if (which == 1) {
            secondPlayerImg.setColorFilter(Color.BLACK, PorterDuff.Mode.OVERLAY);
        }
        if (which == 2) {
            thirdPlayerImg.setColorFilter(Color.BLACK, PorterDuff.Mode.OVERLAY);
        }
    }

    //Blackens the images of the enemy Lehrer, if it died. It's only visually to help the
    // Player understand which Lehrer the enemy has left
    private void greyOutImageEnemy(int which) {
        if (which == 0) {
            aktEnemyImg.setColorFilter(Color.BLACK, PorterDuff.Mode.OVERLAY);
        }
        if (which == 1) {
            secondEnemyImg.setColorFilter(Color.BLACK, PorterDuff.Mode.OVERLAY);
        }
        if (which == 2) {
            thirdEnemyImg.setColorFilter(Color.BLACK, PorterDuff.Mode.OVERLAY);
        }
    }

    //Draws the image for every Lehrer
    private void setImagesForPlayerLehrer() {
        aktPlayerImg.setImageResource(getResources().getIdentifier(usedLehrerList[0].image, getString(R.string.image_type_drawable), getPackageName()));
        secondPlayerImg.setImageResource(getResources().getIdentifier(usedLehrerList[1].image, getString(R.string.image_type_drawable), getPackageName()));
        thirdPlayerImg.setImageResource(getResources().getIdentifier(usedLehrerList[2].image, getString(R.string.image_type_drawable), getPackageName()));
        checkImageState();
    }

    //Draws the image for every enemy Lehrer
    private void setImagesForEnemyLehrer() {
        aktEnemyImg.setImageResource(getResources().getIdentifier(usedEnemyLehrerList[0].image, getString(R.string.image_type_drawable), getPackageName()));
        secondEnemyImg.setImageResource(getResources().getIdentifier(usedEnemyLehrerList[1].image, getString(R.string.image_type_drawable), getPackageName()));
        thirdEnemyImg.setImageResource(getResources().getIdentifier(usedEnemyLehrerList[2].image, getString(R.string.image_type_drawable), getPackageName()));
        checkImageState();

        //If one of the enemy Lehrer has died, blacken it a little
        for (int i = 0; i < 3; i++) {
            if (healthEnemyLehrer[i] <= 0) {
                greyOutImageEnemy(i);
            }
        }
    }

    //If there's an error while loading the Lehrer images on either side, replace it with a placeholder
    private void checkImageState() {
        if (aktPlayerImg.getDrawable() == null) {
            aktPlayerImg.setImageResource(getResources().getIdentifier(getString(R.string.image_placeholder), getString(R.string.image_type_drawable), getPackageName()));
        }
        if (secondPlayerImg.getDrawable() == null) {
            secondPlayerImg.setImageResource(getResources().getIdentifier(getString(R.string.image_placeholder), getString(R.string.image_type_drawable), getPackageName()));
        }
        if (thirdPlayerImg.getDrawable() == null) {
            thirdPlayerImg.setImageResource(getResources().getIdentifier(getString(R.string.image_placeholder), getString(R.string.image_type_drawable), getPackageName()));
        }
        if (aktEnemyImg.getDrawable() == null) {
            aktEnemyImg.setImageResource(getResources().getIdentifier(getString(R.string.image_placeholder), getString(R.string.image_type_drawable), getPackageName()));
        }
        if (secondEnemyImg.getDrawable() == null) {
            secondEnemyImg.setImageResource(getResources().getIdentifier(getString(R.string.image_placeholder), getString(R.string.image_type_drawable), getPackageName()));
        }
        if (thirdEnemyImg.getDrawable() == null) {
            thirdEnemyImg.setImageResource(getResources().getIdentifier(getString(R.string.image_placeholder), getString(R.string.image_type_drawable), getPackageName()));
        }
    }

    //The biggest Method where all the Magic happens. The method the handles incoming data send by the other device
    @Override
    protected void onResume() {
        super.onResume();
        //Handler is used to retrieve the Message coming from the Bluetooth connection
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                //What is the message about
                switch (msg.what) {
                    //If it's about the battle go on
                    case MessageConstants.MESSAGE_READ:
                        //Extracts the message in the data and checks if it suits the requirements
                        String command = (String) msg.obj;
                        if (!command.isEmpty() && command.length() > 4) {
                            String first = command.substring(0, 3);
                            String last = command.substring(4);
                            //The first message send will always contain the enemy's device name to show the Player to which device
                            //he's connected to
                            if (phase == 0) {
                                if (first.equals("DEV")) {
                                    runOnUiThread(() -> Toast.makeText(Battle.this, getString(R.string.other_device_name_message) + last, Toast.LENGTH_LONG).show());

                                    //Sends every selected Lehrer in the array to the enemy, so that he can fill his enemy Lehrer Array
                                    for (int i = 0; i < 3; i++) {
                                        String leh = getString(R.string.bluetooth_message_leh) + usedLehrerList[i].id;
                                        byte[] bytes1 = leh.getBytes(Charset.defaultCharset());
                                        mmConnectedThread.write(bytes1);
                                        try {
                                            Thread.sleep(30);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    playerHealth.setText(getString(R.string.lehrer_health) + healthLehrer[0]);
                                    attack1Name.setText(usedLehrerList[0].attackList.get(0).name);
                                    attack2Name.setText(usedLehrerList[0].attackList.get(1).name);
                                    phase++;
                                } else {
                                    runOnUiThread(() -> Toast.makeText(Battle.this, getString(R.string.command_not_found), Toast.LENGTH_LONG).show());
                                }
                            } else {
                                switch (first) {
                                    //If the enemy send over an Lehrer
                                    case "LEH":
                                        //Go through the whole Lehrer Array and check if the given Lehrer ID matches an Lehrer
                                        //If so add the Lehrer to the enemyLehrerList and also add it's health to it
                                        for (int i = 0; i < allLehrerList.length; i++) {
                                            int num = Integer.parseInt(last);
                                            if (num == allLehrerList[i].id) {
                                                if (usedEnemyLehrerList[countLehrer] == null || usedEnemyLehrerList[countLehrer].id != allLehrerList[i].id) {
                                                    usedEnemyLehrerList[countLehrer] = allLehrerList[i];
                                                    healthEnemyLehrer[countLehrer] = usedEnemyLehrerList[countLehrer].health;
                                                    countLehrer++;
                                                }
                                            }
                                        }
                                        //If the enemy Lehrer Array is full, update the UI for the enemy side
                                        if (usedEnemyLehrerList[0] != null && usedEnemyLehrerList[1] != null && usedEnemyLehrerList[2] != null) {
                                            enemyAction.setText(usedEnemyLehrerList[0].lastName + getString(R.string.lehrer_change_enter_battle_message));
                                            playerAction.setText(usedLehrerList[0].lastName + getString(R.string.lehrer_change_enter_battle_message));

                                            setImagesForPlayerLehrer();
                                            setImagesForEnemyLehrer();
                                            enemyHealth.setText(getString(R.string.lehrer_health) + healthEnemyLehrer[0]);
                                        }
                                        break;
                                    //If the enemy send over an attack
                                    case "ATT":
                                        //The enemy has attacked, now it's the Player's turn
                                        enemyHasAttacked = true;
                                        battleHasBegun = false;
                                        int aktHealth = healthLehrer[0];

                                        //Checks which attack from the enemy Lehrer got send
                                        //Go on with the calculation and UI update for the right Attack
                                        switch (Integer.parseInt(last)) {
                                            case 1:
                                                calculateDmgDone(0, aktHealth);
                                                setEnemyActionText(0);
                                                break;
                                            case 2:
                                                calculateDmgDone(1, aktHealth);
                                                setEnemyActionText(1);
                                                break;
                                            default:
                                                break;
                                        }
                                        changeTurnViewVisibility();
                                        break;
                                    //If the enemy has attacked, update it
                                    case "CTA":
                                        enemyHasAttacked = true;
                                        battleHasBegun = false;
                                        setEnemyActionText(Integer.parseInt(last));
                                        changeTurnViewVisibility();
                                        break;
                                    //If the wants to update the health of a specific Lehrer
                                    case "HPE":
                                        int which = Integer.parseInt(last.substring(0, 1));
                                        int amount = Integer.parseInt(last.substring(1));
                                        healthEnemyLehrer[which] = amount;
                                        setImagesForEnemyLehrer();
                                        break;
                                    //If the enemy simply wants to update the current Lehrer health
                                    case "HPL":
                                        healthEnemyLehrer[0] = Integer.parseInt(last);
                                        enemyHealth.setText(getString(R.string.lehrer_health) + healthEnemyLehrer[0]);
                                        setImagesForEnemyLehrer();
                                        break;
                                    //If the enemy has lost
                                    case "WIN":
                                        gameOver(true);
                                        break;
                                    //If the enemy has changed his current Lehrer
                                    case "NEW":
                                        enemyHasAttacked = true;
                                        battleHasBegun = false;

                                        //Go through the enemy Lehrer Array and check if the given Lehrer ID matches
                                        //If so swap their position in the array
                                        for (int i = 0; i < usedEnemyLehrerList.length; i++) {
                                            if (usedEnemyLehrerList[i].id == Integer.parseInt(last)) {
                                                Lehrer temp = usedEnemyLehrerList[0];
                                                usedEnemyLehrerList[0] = usedEnemyLehrerList[i];
                                                usedEnemyLehrerList[i] = temp;

                                                int tmp = healthEnemyLehrer[0];
                                                healthEnemyLehrer[0] = healthEnemyLehrer[i];
                                                healthEnemyLehrer[i] = tmp;
                                            }
                                        }

                                        changeEnemyLehrerAfterSwap();
                                        changeTurnViewVisibility();
                                        break;
                                    //If the Battle Activity of the enemy has been closed early, cancel the Bluetooth connection
                                    //and return to the Homescreen
                                    case "STP":
                                        mmConnectedThread.cancel();
                                        finish();
                                        break;
                                    default:
                                        runOnUiThread(() -> Toast.makeText(Battle.this, getString(R.string.command_not_found), Toast.LENGTH_LONG).show());
                                        break;
                                }
                            }

                        } else {
                            runOnUiThread(() -> Toast.makeText(Battle.this, getString(R.string.command_not_correct), Toast.LENGTH_SHORT).show());
                            break;
                        }
                        break;
                    default:
                        break;

                }
            }
        };
    }


    //If the Activity is closed early, let the enemy device know and cancel the Bluetooth connection
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mmConnectedThread != null && mmConnectedThread.isAlive()) {
            String cancelMsg = getString(R.string.bluetooth_message_stp);
            byte[] bytes = cancelMsg.getBytes(Charset.defaultCharset());
            mmConnectedThread.write(bytes);

            try {
                Thread.sleep(30);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mmConnectedThread.cancel();
            //finish();
        }
    }

    // Ask if the device can be discovered by other devices for 120 seconds and creates the Server
    public void onStartClick(View view) {
        Intent discoverable = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        startActivityForResult(discoverable, 0);

        AcceptThread acceptThread = new AcceptThread();
        acceptThread.start();
    }

    public void onPairClick(View view) {
        // Companion Device Manager is used to pair with the other Bluetooth device and if needed Pair with it
        CompanionDeviceManager deviceManager = (CompanionDeviceManager) getSystemService(Context.COMPANION_DEVICE_SERVICE);

        // Add a Regex expression to Filter out Mac-Addresses
        BluetoothDeviceFilter deviceFilter = new BluetoothDeviceFilter.Builder().setNamePattern(Pattern.compile("^[A-Za-z\\s.\\(\\)0-9]{3,}$")).build();
        //BluetoothDeviceFilter deviceFilter = new BluetoothDeviceFilter.Builder().build();
        AssociationRequest pairingRequest = new AssociationRequest.Builder().addDeviceFilter(deviceFilter).setSingleDevice(false).build();
        deviceManager.associate(pairingRequest, new CompanionDeviceManager.Callback() {
            @Override
            public void onDeviceFound(IntentSender chooserLauncher) {
                try {
                    startIntentSenderForResult(chooserLauncher, 0, null, 0, 0, 0);
                } catch (IntentSender.SendIntentException e) {
                    Toast.makeText(Battle.this, getString(R.string.bluetooth_try_again_message), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(CharSequence error) {

                runOnUiThread(() -> Toast.makeText(Battle.this, getString(R.string.bluetooth_try_again_message), Toast.LENGTH_LONG).show());
            }
        }, null);

    }

    // Method is called after clicking on a device you want to connect to
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_CODE && data != null) {
            // Retrieves the device from the given data
            BluetoothDevice deviceToPair = data.getParcelableExtra(CompanionDeviceManager.EXTRA_DEVICE);
            if (deviceToPair != null) {
                // Bonds with the device and starts to create a Bluetooth connection with it
                deviceToPair.createBond();
                btDevice = deviceToPair;
                ConnectThread connectThread = new ConnectThread(btDevice);
                connectThread.start();

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    //Message Constants used to specify the type of Message
    private interface MessageConstants {
        int MESSAGE_READ = 0;

    }

    // This Class handles the Server side from the Bluetooth Connection
    private class AcceptThread extends Thread {
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            BluetoothServerSocket tmp = null;

            try {
                tmp = BluetoothAdapter.getDefaultAdapter().listenUsingRfcommWithServiceRecord(TAG, UUID.fromString(MY_UUID));
            } catch (IOException e) {
                Log.e(TAG, getString(R.string.bluetooth_listen_failed), e);
            }
            mmServerSocket = tmp;
        }

        public void run() {
            BluetoothSocket socket;

            // Listens for incoming Connections and tries to make a connection with the Client
            while (true) {
                try {
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    Log.e(TAG, getString(R.string.bluetooth_accept_failed), e);
                    break;
                }

                if (socket != null) {
                    // If the socket isn't empty, hand it over to the Bluetooth Manager
                    connected(socket);
                    // After connecting with the Client, close the Listening socket so that no one else can connect
                    try {
                        mmServerSocket.close();
                    } catch (IOException e) {
                        Log.e(TAG, getString(R.string.bluetooth_close_failed), e);
                    }
                    break;
                }
            }
        }

        public void cancel() {
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e(TAG, getString(R.string.bluetooth_connect_close_failed));
            }
        }
    }

    // This Class is the Client side of the Bluetooth Connection
    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;

        // Needs a device to connect to which will be given by the parameter
        public ConnectThread(BluetoothDevice device) {
            BluetoothSocket tmp = null;

            try {
                tmp = device.createRfcommSocketToServiceRecord(UUID.fromString(MY_UUID));
            } catch (IOException e) {
                Log.e(TAG, getString(R.string.bluetooth_create_failed), e);
            }
            mmSocket = tmp;
        }

        // Automatically runs this method after the Constructor
        public void run() {
            // Cancels Discovery as it uses lots of resources from Bluetooth
            // By canceling Bluetooth has more memory to work with and the device can't be found anymore
            BluetoothAdapter.getDefaultAdapter().cancelDiscovery();

            try {
                // Try's to connect with the given device
                mmSocket.connect();
            } catch (IOException connectionException) {
                try {
                    mmSocket.close();
                } catch (IOException closeException) {
                    Log.e(TAG, getString(R.string.bluetooth_client_close_failed), closeException);
                }
                return;
            }
            // If the connection succeeds, hand it over to the Bluetooth Manager
            connected(mmSocket);
            battleHasBegun = true;
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, getString(R.string.bluetooth_client_close_failed), e);
            }
        }
    }

    // This class Manages the Bluetooth connection after it has been established
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        // Creates an input and output stream from the given Socket
        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e(TAG, getString(R.string.bluetooth_input_stream_error), e);
            }

            try {
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, getString(R.string.bluetooth_output_stream_error), e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;


        }

        public void run() {
            byte[] mmBuffer = new byte[1024];
            int numBytes;
            // Sends the name of the device to the connected device
            if (phase == 0) {
                String deviceName = getString(R.string.bluetooth_message_dev) + BluetoothAdapter.getDefaultAdapter().getName();
                byte[] bytes = deviceName.getBytes(Charset.defaultCharset());
                mmConnectedThread.write(bytes);

                runOnUiThread(Battle.this::changeActivityLayoutAfterConnect);
            }


            // Waits for incoming Data
            while (true) {
                try {
                    // Read from the InputStream
                    numBytes = mmInStream.read(mmBuffer);
                    // Hands the given Information over to the Handler which manages the data
                    Message readMsg = mHandler.obtainMessage(MessageConstants.MESSAGE_READ, numBytes, -1, mmBuffer);
                    readMsg.obj = new String(mmBuffer, 0, numBytes);
                    readMsg.sendToTarget();
                } catch (IOException e) {
                    Log.e(TAG, getString(R.string.bluetooth_input_stream_disconnected), e);
                    break;
                }
            }
        }

        // Method is called to send Data to the connected device
        public void write(byte[] bytes) {
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) {
                Log.e(TAG, getString(R.string.bluetooth_send_data_error), e);
            }
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, getString(R.string.bluetooth_client_close_failed), e);
            }
        }
    }

    // Starts the Bluetooth Manager which handles the connection between devices
    private void connected(BluetoothSocket socket) {
        mmConnectedThread = new ConnectedThread(socket);
        mmConnectedThread.start();
    }
}
