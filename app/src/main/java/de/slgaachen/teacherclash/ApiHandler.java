package de.slgaachen.teacherclash;
//Fynn
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

class News {
    String title;
    String subtitle;
    String imageUrl;

    News(String _title, String _subtitle, String _imageUrl) {
        title = _title;
        subtitle = _subtitle;
        imageUrl = _imageUrl;
    }
}

class ApiHandler {

    static ArrayList<News> newsList = new ArrayList<News>();


    public void getNews() {
        Log.v("test1","TESTJAN 123");

        try {
            URL url = new URL("https://testing.vasconezgerlach.de/slg-teacherfights/093824029834092834098224/news.json");

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();

//Getting the response code
            int responsecode = conn.getResponseCode();

            if (responsecode != 200) {
                throw new RuntimeException("HttpResponseCode: " + responsecode);
            } else {

                String inline = "";
                Scanner scanner = new Scanner( url.openStream());

                //Write all the JSON data into a string using a scanner
                while (scanner.hasNext()) {
                    inline += scanner.nextLine();
                }

                //Close the scanner
                scanner.close();

                //Using the JSON simple library parse the string into a json object
                JSONParser parse = new JSONParser();
                JSONObject data_obj = (JSONObject) parse.parse(inline);

                //Get the required object from the above created object
                JSONObject obj = (JSONObject) data_obj.get("Global");

                //Get the required data using its key
                System.out.println("TESTJAN" + obj.get("TotalRecovered").toString());
            }
        }
            catch (Exception e) {
            Log.e("Exception", e.toString());
        }
    }
}