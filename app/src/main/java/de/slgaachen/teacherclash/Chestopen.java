package de.slgaachen.teacherclash;
//Jan
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class Chestopen extends AppCompatActivity implements View.OnClickListener {

    private Lehrer gezogen1;
    private Lehrer gezogen2;
    private Lehrer gezogen3;
    private Lehrer gezogen4;
    private int Seltenheitm; //fürs Money abziehen
    private User user = new User(Chestopen.this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chestopen);

        Button backtoshop = findViewById(R.id.backtoshop);
        ImageButton slot1 = findViewById(R.id.card1_chestopen);
        ImageButton slot2 = findViewById(R.id.card2_chestopen);
        ImageButton slot3 = findViewById(R.id.card3_chestopen);
        ImageButton slot4 = findViewById(R.id.card4_chestopen);
        TextView test = findViewById(R.id.TextView_caseopen);

        backtoshop.setOnClickListener(this);
        slot1.setOnClickListener(this);
        slot2.setOnClickListener(this);
        slot3.setOnClickListener(this);
        slot4.setOnClickListener(this);

        gezogen1 = randomLehrer();
        gezogen2 = randomLehrer();
        gezogen3 = randomLehrer();
        gezogen4 = randomLehrer();

        int Seltenheit = getIntent().getIntExtra("Seltenheit",0);
        aufbauen(Seltenheit);
        Seltenheitm = Seltenheit;
       // test.setText(gezogen4.getFreigeschaltet().toString()); //Überprüfung



    }
    public void aufbauen(int pSeltenheit) {
        ImageButton slot1 = findViewById(R.id.card1_chestopen);
        ImageButton slot2 = findViewById(R.id.card2_chestopen);
        ImageButton slot3 = findViewById(R.id.card3_chestopen);
        ImageButton slot4 = findViewById(R.id.card4_chestopen);
        TextView test = findViewById(R.id.TextView_caseopen);

      if(pSeltenheit == 1) {
          slot1.setImageResource(R.drawable.logo);
          slot1.setBackgroundResource(R.color.logoblue);

          slot2.setImageResource(R.drawable.logo);
          slot2.setBackgroundResource(R.color.logoblue);

          slot3.setClickable(false);
          slot3.setVisibility(View.INVISIBLE);

          slot4.setClickable(false);
          slot4.setVisibility(View.INVISIBLE);

          test.setText(String.valueOf(pSeltenheit));
      }
      else if (pSeltenheit == 2) {
          slot1.setImageResource(R.drawable.logo_pink);
          slot1.setBackgroundResource(R.color.logopink);

          slot2.setImageResource(R.drawable.logo_pink);
          slot2.setBackgroundResource(R.color.logopink);

          slot3.setImageResource(R.drawable.logo_pink);
          slot3.setBackgroundResource(R.color.logopink);

          slot4.setClickable(false);
          slot4.setVisibility(View.INVISIBLE);

          test.setText(String.valueOf(pSeltenheit));
      }
      else if (pSeltenheit == 3) {
          slot1.setImageResource(R.drawable.logo_yellow);
          slot1.setBackgroundResource(R.color.logoyellow);

          slot2.setImageResource(R.drawable.logo_yellow);
          slot2.setBackgroundResource(R.color.logoyellow);

          slot3.setImageResource(R.drawable.logo_yellow);
          slot3.setBackgroundResource(R.color.logoyellow);

          slot4.setImageResource(R.drawable.logo_yellow);
          slot4.setBackgroundResource(R.color.logoyellow);

          test.setText(String.valueOf(pSeltenheit));
      }
    }



    @Override
    public void onClick(View v) {
        ImageButton slot1 = findViewById(R.id.card1_chestopen);
        ImageButton slot2 = findViewById(R.id.card2_chestopen);
        ImageButton slot3 = findViewById(R.id.card3_chestopen);
        ImageButton slot4 = findViewById(R.id.card4_chestopen);
        TextView test = findViewById(R.id.TextView_caseopen);

        if (v.getId() == R.id.backtoshop) {
            if(Seltenheitm == 1) {
                user.robMoney(500);
            } else if(Seltenheitm == 2) {
                user.robMoney(1000);
            } else {
                user.robMoney(1500);
            }
            user.safemoney();

            Intent i = new Intent(Chestopen.this, Shop.class);
            startActivity(i);

        }

        if (v.getId() == R.id.card1_chestopen) {
            TextView angabe_karte1 = findViewById(R.id.angabe_karte1);
            slot1.setBackground(null);
            String img1 = gezogen1.getImage();
            slot1.setImageResource(getResources().getIdentifier(img1,"drawable", getPackageName()));
            if (gezogen1.getFreigeschaltet() != true){
                gezogen1.setFreigeschaltet();
                user.einfügen(gezogen1);
                user.saveTeachers();
                angabe_karte1.setText(" NEU!");
            }
            else {
                angabe_karte1.setText(" kein Glück :( ");
            }

           // test.setText(img1);
        }
        if (v.getId() == R.id.card2_chestopen) {
            TextView angabe_karte2 = findViewById(R.id.angabe_karte2);
            slot2.setBackground(null);
            String img2 = gezogen2.getImage();
            slot2.setImageResource(getResources().getIdentifier(img2,"drawable", getPackageName()));
            if (gezogen2.getFreigeschaltet() != true){
                gezogen2.setFreigeschaltet();
                user.einfügen(gezogen2);
                user.saveTeachers();
                angabe_karte2.setText(" NEU!");
            }
            else {
                angabe_karte2.setText(" kein Glück :( ");
            }
        }
        if (v.getId() == R.id.card3_chestopen) {
            TextView angabe_karte3 = findViewById(R.id.angabe_karte3);
            String img3 = gezogen3.getImage();
            slot3.setBackground(null);
            slot3.setImageResource(getResources().getIdentifier(img3,"drawable", getPackageName()));
            if (gezogen3.getFreigeschaltet() != true){
                // Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
                gezogen3.setFreigeschaltet();
                user.einfügen(gezogen3);
                user.saveTeachers();
                angabe_karte3.setText(" NEU!");
            }
            else {
                angabe_karte3.setText(" kein Glück :( ");
            }
        }
        if (v.getId() == R.id.card4_chestopen) {
            TextView angabe_karte4 = findViewById(R.id.angabe_karte4);
            slot4.setBackground(null);
            String img4 = gezogen4.getImage();
            slot4.setImageResource(getResources().getIdentifier(img4,"drawable", getPackageName()));
            if (gezogen4.getFreigeschaltet() != true){
               // Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
                gezogen4.setFreigeschaltet();
                user.einfügen(gezogen4);
                user.saveTeachers();
                angabe_karte4.setText(" NEU!");
            }
            else {
                angabe_karte4.setText(" kein Glück :( ");
            }
            //test.setText(gezogen4.getFreigeschaltet().toString());  //Überprüfung
        }

    }



    public Lehrer randomLehrer(){
       return user.getRandomLehrer();
    }


}