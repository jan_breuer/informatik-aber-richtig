package de.slgaachen.teacherclash;
//Fynn + (Jan)
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.ArraySet;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class User extends AppCompatActivity {
    private Context context;

    private SharedPreferences sharedPreferences;
    private static ArrayList<Lehrer> lehrerList = new ArrayList<>();
    private static ArrayList<String> savedTeacherIds = new ArrayList<>();
    private static int money;
   // TinyDB tinydb = new TinyDB(context);


    User(Context pContext){
        context = pContext;
    }
    public boolean checkIflehrerListeisempty(){
        boolean a = false ;
        if(lehrerList.size() == 0){
            a =true;
        }
        return a;
    }

    public void robMoney(int amount){
        money = money - amount;
    }

    public int getMoney(){
        return money;
    }
    public void addMoney(int amount){
        money = money + amount;
    }
    public void setMoney(int amount){
        money = amount;
    }

    public void safemoney(){
        sharedPreferences = context.getSharedPreferences("money", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("key_money", getMoney());
        editor.apply();
    }
    public void getsavedmoney(){
        sharedPreferences = context.getSharedPreferences("money", Context.MODE_PRIVATE);
        setMoney(sharedPreferences.getInt("key_money", 0));
    }

    //get teachers from server and synchronise with persistent data
    public void syncTeachers() {
        getTeachersFromServer();
        savedTeacherIds = getPersistentList();
        checkMyTeachers();
        // store preference
       // setStringArrayPref(this, "urls", savedTeacherIds);

        // retrieve preference
       // savedTeacherIds = getStringArrayPref(this, "urls");
    }

    //get teachers from server
    private void getTeachersFromServer(){
     addLehrer();
    }

    //checks if teacher is owned by user
    private void checkMyTeachers(){
        //goes through all teachers
        for(int i = 0; i < lehrerList.size(); i++)
        {
            //goes through all saved teacher ids
            for(int j = 0; j < savedTeacherIds.size(); j++) {
                //check if teacher id matches the id of an owned teacher
                if(String.valueOf(lehrerList.get(i).getId()) == savedTeacherIds.get(j)) {
                    //set teacher as owned
                    lehrerList.get(i).setFreigeschaltet(); //true
                }
            }
        }
    }

    //gets teachers ids that the user owns
    private ArrayList<String> getPersistentList() {
        sharedPreferences = context.getSharedPreferences("savedTeacherIds", Context.MODE_PRIVATE);
        Set<String> ids = new HashSet<>();
        ArrayList<String> list = new ArrayList<>(sharedPreferences.getStringSet("savedTeacherIds_key",ids));
        return list;

//        //sharedPreferences = this.getSharedPreferences("savedTeacherIds", Context.MODE_PRIVATE);
//
//        // retrieve data
//
//        ArrayList<String> storeData = new ArrayList<>();
//
//        try {
//            storeData = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("savedTeacherIds",ObjectSerializer.serialize(new ArrayList<>())));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        //Toast.makeText(getApplicationContext(),storeData.toString(),Toast.LENGTH_LONG).show();
//
//        return  storeData;
//
//        //return  storeData = tinydb.getListString("MyUsers");
//
//

    }

    public void saveTeachers() {
        sharedPreferences = context.getSharedPreferences("saveTeacherIds", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Set<String> ids = new HashSet<>(getMyTeacherIds());
        Log.e("test", "Zu speichernde Lehrer: " + ids.toString());
        editor.putStringSet("savedTeacherIds_key",ids);
        editor.apply();

        //setStringArrayPref(this, "urls", savedTeacherIds);

//        savedTeacherIds = getMyTeacherIds();
//       // sharedPreferences = this.getSharedPreferences("savedTeacherIds", Context.MODE_PRIVATE);
//
//        //store data
//
//        try {
//            SharedPreferences.Editor editor = sharedPreferences.edit();
//            editor.putString("savedTeacherIds", ObjectSerializer.serialize(savedTeacherIds)).apply();
//            editor.apply();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


       // tinydb.putListString("MyUsers", savedTeacherIds);
    }

    public ArrayList<String> getMyTeacherIds() {
        savedTeacherIds.clear();
        ArrayList<String> teacherIdList = new ArrayList<>(); //0

        for(int i = 0; i < lehrerList.size(); i++) //4
        {
            //check if teacher is owned by user
            if(lehrerList.get(i).getFreigeschaltet()) {

                //set teacher as owned
                teacherIdList.add(String.valueOf(lehrerList.get(i).getId())); //1
            }
        }

        return teacherIdList;
    }

    public void einfügen (Lehrer plehrer){
        for(int i = 0; i< lehrerList.size(); i++){
            if(lehrerList.get(i).getId() == plehrer.getId()){
                lehrerList.add(plehrer);
                lehrerList.remove(i);
            }

        }

    }

    public ArrayList<Lehrer> getLehrer() {
        //addLehrer();
        return lehrerList;
    }

    public Lehrer getRandomLehrer() {
       Random r = new Random();
       int i = r.nextInt(lehrerList.size());
       Lehrer lehrerxy = lehrerList.get(i);
       return lehrerxy;

   }

    public void addLehrer() {
        ArrayList<Attack> attackList = new ArrayList<Attack>();
        ArrayList<Attack> attackList1 = new ArrayList<Attack>();
        ArrayList<Attack> attackList2 = new ArrayList<Attack>();
        ArrayList<Attack> attackList3 = new ArrayList<Attack>();
        ArrayList<Attack> attackList4 = new ArrayList<Attack>();


        attackList.add(new Attack(7,"Problem an der Kiste",true));
        attackList.add(new Attack(5,"Seelenfrieden",false));
        lehrerList.add(new Lehrer(
                "Jochen",
                "Remy",
                "Religions Lehrer",
                100,
                0,
                true,
                attackList,
                "remy1ueberarbeitet"
        ));

        attackList1.add(new Attack(11, "Nerf Gun", true));
        attackList1.add(new Attack(15, "Heißer Kaffe", false));

        lehrerList.add(new Lehrer(
                "Klaus",
                "Mulorz",
                "Physik Lehrer",
                75,
                1,
                false,
                attackList1,
                "mulortz"
        ));

        attackList2.add(new Attack(6, "Handy abnehmen", true));
        attackList2.add(new Attack(4, "Regelheft lesen", false));

        lehrerList.add(new Lehrer(
                "Dr. Manuela",
                "Weißenbach",
                "Biologie Lehrerin",
                110,
                2,
                false,
                attackList2,
                "weissenbach"
        ));

        attackList3.add(new Attack(11, "Fahrrad-Drive-By", true));
        attackList3.add(new Attack(2, "Anekdote", false));

        lehrerList.add(new Lehrer(
                        "Roman",
                        "Thürwächter",
                        "Deutsch Lehrer",
                        105,
                        3,
                        false,
                        attackList3,
                       "romanthuerwaechter"
        ));

        attackList4.add(new Attack(9, "Lat. Beleidigen", true));
        attackList4.add(new Attack(8, "Matherätsel", false));

        lehrerList.add(new Lehrer(
                "Miriam",
                "Behl",
                "Mathe Lehrerin",
                80,
                4,
                false,
                attackList4,
                "behl"
        ));
    }
    /*
    public static ArrayList<String> getStringArrayPref(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String json = prefs.getString(key, null);
        ArrayList<String> urls = new ArrayList<String>();
        if (json != null) {
            try {
                JSONArray a = new JSONArray(json);
                for (int i = 0; i < a.length(); i++) {
                    String url = a.optString(i);
                    urls.add(url);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return urls;
    }

     */

    /*public static void setStringArrayPref(Context context, String key, ArrayList<String> values) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        JSONArray a = new JSONArray();
        for (int i = 0; i < values.size(); i++) {
            a.put(values.get(i));
        }
        if (!values.isEmpty()) {
            editor.putString(key, a.toString());
        } else {
            editor.putString(key, null);
        }
        editor.commit();
    }

     */


}
