package de.slgaachen.teacherclash;
//Noah
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.widget.AdapterView;
import android.widget.Toast;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Inventar extends AppCompatActivity {
    float y1,y2,x1,x2;
   /* public ExpandableListView expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
    public ExpandableListAdapter expandableListAdapter;
    public List<String> expandableListTitle;
    public HashMap<String, List<String>> expandableListDetail = ExpandableListDataPump.getData();*/

    public static final String LOG = Inventar.class.getSimpleName();
    private List<String> mSampleQuoteList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventar);
        createQuoteList();
        bindAdapterToListView();







        /*expandableListTitle = new ArrayList<>(expandableListDetail.keySet());
        expandableListAdapter = new CustomExpandableListAdapter(this, expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        expandableListTitle.get(groupPosition) + " List Expanded.",
                        Toast.LENGTH_SHORT).show();
            }
        });
        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        expandableListTitle.get(groupPosition) + " List Collapsed.",
                        Toast.LENGTH_SHORT).show();

            }
        });

        /*expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
            Toast.makeText(
                    getApplicationContext(),
                    expandableListTitle.get(groupPosition)
                            + " -> "
                            + expandableListDetail.get(
                            expandableListTitle.get(groupPosition)).get(
                            childPosition),
                    Toast.LENGTH_SHORT
            ).show();
                return false;
            }
        });*/

        setContentView(R.layout.activity_inventar);
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.nav_invent);
        bottomNavigationView.setItemIconTintList(null);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.nav_home:
                        startActivity(new Intent(getApplicationContext(),Homescreen.class));
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        return true;

                    case R.id.nav_shop:
                        startActivity(new Intent(getApplicationContext(),Shop.class));
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        return true;

                }
                return false;
            }
        });


    }
    private void createQuoteList() {

        String[] Abilities = getResources().getStringArray(R.array.Abilities);
        mSampleQuoteList = new ArrayList<>(Arrays.asList(Abilities));
    }

    private void bindAdapterToListView() {

        ArrayAdapter<String> TeacherArrayAdapter =
                new ArrayAdapter<>(
                        Inventar.this, // Die aktuelle Umgebung (diese Activity)
                        R.layout.list_row, // Die ID des Zeilenlayouts (der XML-Layout Datei)
                        R.id.Remy,   // Die ID eines TextView-Elements im Zeilenlayout
                        mSampleQuoteList); // Beispieldaten in einer ArrayList

        ListView quoteListView = (ListView) findViewById(R.id.listview_activity_inventar);
        quoteListView.setAdapter(TeacherArrayAdapter);
    }





    public boolean onTouchEvent(MotionEvent touchEvent){
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        switch(touchEvent.getAction()){
            case MotionEvent.ACTION_DOWN:
                x1 = touchEvent.getX();
                y1 = touchEvent.getY();
                break;
            case MotionEvent.ACTION_UP:
                x2 = touchEvent.getX();
                y2 = touchEvent.getY();
                if(x1 < x2){
                    Intent i = new Intent(Inventar.this, Homescreen.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                }
                break;
        }
        return false;
    }





}