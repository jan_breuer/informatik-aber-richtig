package de.slgaachen.teacherclash;
//Jan
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class fragmentsettings extends Fragment implements View.OnClickListener {
    Button btn;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_fragmentsettings, container,false);
        btn = (Button) rootView.findViewById(R.id.freemoney);
        btn.setOnClickListener(this);
        return rootView;

    }

    @Override
    public void onClick(View v) {

        new User(getContext()).addMoney(500);
    }
}