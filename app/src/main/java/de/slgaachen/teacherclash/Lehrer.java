package de.slgaachen.teacherclash;
//Fynn
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class Lehrer extends AppCompatActivity {
    //Generelle Daten über den de.slgaachen.teacherclash.Lehrer
    String firstName;
    String lastName;
    String description;
    String image;
    Boolean freigeschaltet;

    int health;
    int id;
    User user = new User(this);



    //Alle Attacken vom de.slgaachen.teacherclash.Lehrer
    ArrayList<Attack> attackList = new ArrayList<>(4);


    Lehrer(
            String _firstName,
            String _lastName,
            String _description,
            int _health,
            int _id,
            Boolean _freigeschaltet,
            ArrayList<Attack> _attackList,
            String _image

    ) {
        firstName = _firstName;
        lastName = _lastName;
        description = _description;
        health = _health;
        id = _id;
        attackList = _attackList;
        freigeschaltet = _freigeschaltet;
        image = _image;
    }
    public String getImage() {
        return image;
    }
    public int getId(){
        return id;
    }

    public boolean getFreigeschaltet(){
      return freigeschaltet;
    }

    public void setFreigeschaltet() {
        this.freigeschaltet = true;
    }
}
