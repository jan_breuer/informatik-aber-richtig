package de.slgaachen.teacherclash;
//Jan
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class lostActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lost);

        Intent intent = getIntent();
        int money = intent.getIntExtra("Money", 0);
        TextView moneyTextView = findViewById(R.id.moneytextview);
        moneyTextView.setText(getString(R.string.money_gained) + money + getString(R.string.money_gained2));
    }


    public void onReturnClick(View v) {
        Intent i = new Intent(lostActivity.this,Homescreen.class);
        startActivity(i);
    }
}