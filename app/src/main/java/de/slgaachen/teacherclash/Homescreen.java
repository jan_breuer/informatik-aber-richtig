package de.slgaachen.teacherclash;
//Jan
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

public class Homescreen extends AppCompatActivity {
    float x1,x2,y1,y2;
    User user = new User(Homescreen.this);
    //static boolean start = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView textView = findViewById(R.id.showMoney_homescreen);
        textView.setText("Money: " + user.getMoney());

        if(user.checkIflehrerListeisempty()) {
            user.syncTeachers();
        }

        user.getsavedmoney();

       // Toast.makeText(getApplicationContext(), user.savedTeacherIds.toString(),Toast.LENGTH_LONG).show();



        ImageButton settingsbutton = findViewById(R.id.settingsbutton);
        ImageButton mailbutton = findViewById(R.id.mailbutton);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.nav_home);
        bottomNavigationView.setItemIconTintList(null);

        settingsbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(getSupportFragmentManager().findFragmentById(R.id.framelayout) != null){
                   Fragment fragment = getSupportFragmentManager().findFragmentByTag("targetTAG");
                   if(fragment != null)
                       getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                       TextView textView = findViewById(R.id.showMoney_homescreen);
                       textView.setText("Money: " + user.getMoney());
                       user.safemoney();
             }
               else {
                   replaceFragment(new fragmentsettings());
               }
            }
        });
        mailbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getSupportFragmentManager().findFragmentById(R.id.framelayout) != null){
                    Fragment fragment = getSupportFragmentManager().findFragmentByTag("targetTAG");
                    if(fragment != null)
                        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                  }
                else {
                    replaceFragment(new fragmentmail());
                }
            }
        });

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.nav_shop:
                        startActivity(new Intent(getApplicationContext(),Shop.class));
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        return true;
                    case R.id.nav_invent:
                        startActivity(new Intent(getApplicationContext(),Inventar.class));
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        return true;
                }
                return false;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        TextView money = findViewById(R.id.showMoney_homescreen);
        money.setText("Money: " + user.getMoney());
        user.safemoney();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        user.safemoney();
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.framelayout,fragment, "targetTAG");
        fragmentTransaction.commit();
    }



    public boolean onTouchEvent(MotionEvent touchEvent){
        ApiHandler apiHandler = new ApiHandler();
        apiHandler.getNews();
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);

        switch(touchEvent.getAction()){
            case MotionEvent.ACTION_DOWN:
                x1 = touchEvent.getX();
                y1 = touchEvent.getY();
                break;
            case MotionEvent.ACTION_UP:
                x2 = touchEvent.getX();
                y2 = touchEvent.getY();
                if(x1 > x2){
                Intent i = new Intent(Homescreen.this, Inventar.class);
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }else if(x1 < x2){
               Intent i = new Intent(Homescreen.this, Shop.class);
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

            }
            break;
        }
        return false;
    }

    public void changeToBattleActivity(View view){
        Intent intent = new Intent(this, Battle.class);
        startActivity(intent);
    }

}